package com.cx.babastudio.util

import android.opengl.Visibility
import android.view.View
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

fun View.visible(){
    visibility = View.VISIBLE
}

fun View.invsible(){
    visibility = View.INVISIBLE
}

fun currency(number:String?):String{
    var number = number
    if (number != "-"){
        val amount = java.lang.Double.parseDouble(number)
        val simbol = DecimalFormatSymbols()
        val myFormatter: DecimalFormat

        simbol.decimalSeparator = ','
        simbol.groupingSeparator = '.'
        myFormatter = DecimalFormat("###,###,###,###,##0", simbol)
        number = myFormatter.format(amount)
    }
    return number.toString()
}