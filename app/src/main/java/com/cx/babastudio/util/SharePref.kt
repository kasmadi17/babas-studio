package com.cx.babastudio.util

import android.content.Context
import android.content.SharedPreferences

class SharePref(context: Context){

    val prefId="cx.baba.studio"
    private val sp: SharedPreferences = context.getSharedPreferences(prefId, Context.MODE_PRIVATE)
    private val spEditor: SharedPreferences.Editor

    init {
        spEditor = sp.edit()
    }

    fun getString(key:String):String?{
        return sp.getString(key,"")
    }

    fun getBoolean(key:String):Boolean{
        return sp.getBoolean(key,false)
    }

    fun saveBoolean(key:String,value:Boolean){
        spEditor.putBoolean(key,value)
        spEditor.commit()
    }

    fun saveString(key:String,value:String){
        spEditor.putString(key,value)
        spEditor.commit()
    }

    fun clear(){
        spEditor.clear()
        spEditor.apply()
    }
}
