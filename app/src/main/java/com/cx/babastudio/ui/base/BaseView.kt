package com.cx.babastudio.ui.base

interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showError()
    fun onAttachView()
    fun onDetachView()
}