package com.cx.babastudio.ui.material

import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cx.babastudio.R
import com.cx.babastudio.model.material.SectionsItem
import org.jetbrains.anko.find

class MaterialAdapter(private val data:List<SectionsItem>
                      ,private val listener:(SectionsItem)->Unit)
    :RecyclerView.Adapter<MaterialAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view:View = LayoutInflater.from(p0.context).inflate(R.layout.list_material,p0,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int= data.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bindData(data[p1],listener)
    }

    inner class MyViewHolder(view:View):RecyclerView.ViewHolder(view){
        private var title:TextView =view.find(R.id.txtTitle)

        fun bindData(data:SectionsItem,listener: (SectionsItem) -> Unit){
            title.text = data.title
            itemView.setOnClickListener {
                listener(data)
            }
        }

    }
}