package com.cx.babastudio.ui.main

import com.cx.babastudio.model.Course
import com.cx.babastudio.model.MyCourse
import com.cx.babastudio.network.ApiClient
import com.cx.babastudio.ui.base.BasePresenter
import com.cx.babastudio.ui.base.BaseView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter:BasePresenter<MainView> {
    private var mView:MainView?=null
    override fun onAttach(view: MainView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun getCouser(token:String){
        mView?.showProgress()
        val call:Call<Course> = ApiClient.retrofit.getCouser(token)
        call.enqueue(object : Callback<Course>{
            override fun onFailure(call: Call<Course>, t: Throwable) {
                mView?.hideProgress()
                mView?.showError()
            }

            override fun onResponse(call: Call<Course>, response: Response<Course>) {
                if (response.isSuccessful){
                    mView?.hideProgress()
                    val data = response.body()?.myCourse
                    val viewCourse = response.body()?.getViewCourse
                    data?.let { mView?.showCourse(it) }
                    viewCourse?.let { mView?.showViewCourse(it) }

                }
            }

        })
    }

    fun logout(token: String){
        mView?.showProgress()
        val call:Call<ResponseBody> = ApiClient.retrofit.logout(token)
        call.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mView?.hideProgress()
                mView?.showError()
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful){
                    mView?.logout()
                }
            }

        })
    }
}