package com.cx.babastudio.ui.mycourse

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cx.babastudio.R
import com.cx.babastudio.model.PackagesItem
import org.jetbrains.anko.find

class MyCourseAdapter(private val list:List<PackagesItem>
                      ,private val listener:(idCourse:Int,idMaterial:Int)->Unit)
    :RecyclerView.Adapter<MyCourseAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyCourseAdapter.MyViewHolder {
        val view:View= LayoutInflater.from(p0.context).inflate(R.layout.list_my_course,p0,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int =list.size

    override fun onBindViewHolder(p0: MyCourseAdapter.MyViewHolder, p1: Int) {
        p0.bindData(list[p1],listener)
    }

    inner class MyViewHolder(view: View):RecyclerView.ViewHolder(view){
        private var title:TextView = view.find(R.id.txtTitle)
        private var duration:TextView = view.find(R.id.txtDuration)

        fun bindData(data:PackagesItem,listener: (idCourse: Int, idMaterial: Int) -> Unit){
            title.text =data.title
            val textDuration="Menit "+data.duration
            duration.text = textDuration
            itemView.setOnClickListener {
                listener(0,0)
            }
        }
    }

}