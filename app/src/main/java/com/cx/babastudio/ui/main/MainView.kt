package com.cx.babastudio.ui.main

import com.cx.babastudio.model.Data
import com.cx.babastudio.model.GetViewCourse
import com.cx.babastudio.model.MyCourse
import com.cx.babastudio.ui.base.BaseView

interface MainView:BaseView {
    fun showCourse(data:MyCourse)
    fun showViewCourse(data:GetViewCourse)
    fun logout()
}