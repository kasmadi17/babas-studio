package com.cx.babastudio.ui.material

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.cx.babastudio.R
import com.cx.babastudio.model.Section
import com.cx.babastudio.model.material.Data
import com.cx.babastudio.model.material.SectionsItem
import com.cx.babastudio.ui.material.materialdetail.MaterialDetail
import com.cx.babastudio.util.Constans.TOKEN
import com.cx.babastudio.util.SharePref
import kotlinx.android.synthetic.main.activity_material.*
import org.jetbrains.anko.find
import org.jetbrains.anko.notificationManager
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.toast

class Material : AppCompatActivity(),MaterialView {
    private lateinit var presenter: MaterialPresenter
    private lateinit var sharePref: SharePref
    private lateinit var adapter: MaterialAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var sectionsItem:MutableList<SectionsItem> = mutableListOf()
    private var idMaterial=0
    private var idKursus=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material)
        init()
    }

    override fun showMaterial(data: Data) {
        txtTitle.text = data.courseData?.title
        sectionsItem.clear()
        data.courseData?.sections?.let { sectionsItem.addAll(it) }
        adapter.notifyDataSetChanged()
    }

    override fun showProgress() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showError() {
        toast(R.string.error_msg)
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.getMaterial(sharePref.getString(TOKEN).toString(),idKursus,idMaterial)
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    private fun init(){
        idMaterial = intent.getIntExtra("idMaterial",0)
        idKursus= intent.getIntExtra("idKursus",0)
        sharePref = SharePref(this)
        presenter = MaterialPresenter()
        recyclerView = find(R.id.recycleView)
        swipeRefreshLayout = find(R.id.swipe)
        adapter = MaterialAdapter(sectionsItem){
            startActivity<MaterialDetail>("data" to it)
        }
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimary)
        swipeRefreshLayout.onRefresh {
            presenter.getMaterial(sharePref.getString(TOKEN).toString(),idKursus,idMaterial)
        }

        onAttachView()
    }
}
