package com.cx.babastudio.ui.login

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.cx.babastudio.ui.main.MainActivity
import com.cx.babastudio.R
import com.cx.babastudio.model.Users
import com.cx.babastudio.util.Constans.ISLOGIN
import com.cx.babastudio.util.Constans.TOKEN
import com.cx.babastudio.util.SharePref
import com.cx.babastudio.util.invsible
import com.cx.babastudio.util.visible
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class Login : AppCompatActivity(), LoginView {
    private lateinit var presenter: LoginPresenter
    private lateinit var sharePref: SharePref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter = LoginPresenter()
        sharePref = SharePref(this)
        loading.invsible()
        btnLogin.setOnClickListener {
            if (!txtUsername.text.isNullOrEmpty() && !txtPassword.text.isNullOrEmpty()){
                presenter.login(txtUsername.text.toString(),txtPassword.text.toString())
            }else{
                toast(R.string.username_null)
            }
        }
        onAttachView()
        if (sharePref.getBoolean(ISLOGIN)){
            startActivity<MainActivity>()
        }
    }

    override fun onAttachView() {
        presenter.onAttach(this)
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun showData(data: Users) {
        data.accessToken?.let { sharePref.saveString(TOKEN, "Bearer $it") }
        sharePref.saveBoolean(ISLOGIN,true)
    }

    override fun openHome() {
        startActivity<MainActivity>()
    }

    override fun showProgress() {
        loading.visible()
    }

    override fun hideProgress() {
        loading.invsible()
    }

    override fun showError() {
        toast(R.string.error_msg)
    }
}
