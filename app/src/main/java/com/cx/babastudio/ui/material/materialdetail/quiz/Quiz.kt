package com.cx.babastudio.ui.material.materialdetail.quiz

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.cx.babastudio.R
import com.cx.babastudio.model.material.MaterialsItem
import com.cx.babastudio.model.material.QuizzesItem
import org.jetbrains.anko.find

class Quiz : AppCompatActivity() {
    private lateinit var adapter: QuizAdapter
    private lateinit var recyclerView: RecyclerView
    private var listQuiz:MutableList<QuizzesItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        init()
    }

    private fun init(){
        recyclerView = find(R.id.recycleView)
        val data = intent.getParcelableExtra<MaterialsItem>("data")
        listQuiz.clear()
        data.quizzes?.let { listQuiz.addAll(it) }
        adapter = QuizAdapter(listQuiz)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}
