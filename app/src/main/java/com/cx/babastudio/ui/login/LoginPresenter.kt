package com.cx.babastudio.ui.login

import com.cx.babastudio.model.Users
import com.cx.babastudio.network.ApiClient

import com.cx.babastudio.ui.base.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter:BasePresenter<LoginView> {
    private var mView: LoginView?=null

    override fun onAttach(view: LoginView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun login(username:String,password:String){
        mView?.showProgress()
        val call:Call<Users> = ApiClient.retrofit.login(username, password)
        call.enqueue(object : Callback<Users>{
            override fun onFailure(call: Call<Users>, t: Throwable) {
                mView?.hideProgress()
                mView?.showError()

            }

            override fun onResponse(call: Call<Users>, response: Response<Users>) {
               if (response.isSuccessful){
                   mView?.hideProgress()
                   val data = response.body()?.accessToken
                   if (data!=null){
                       val dataUser = response.body()
                       dataUser?.let { mView?.showData(it) }
                       mView?.openHome()
                   }
               }
            }

        })

    }
}