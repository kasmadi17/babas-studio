package com.cx.babastudio.ui.material.materialdetail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.cx.babastudio.R
import com.cx.babastudio.model.material.MaterialsItem
import org.jetbrains.anko.find

class MaterialDetailAdapter(private val list:List<MaterialsItem>
                            ,private val listener:(MaterialsItem)->Unit)
    :RecyclerView.Adapter<MaterialDetailAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view:View = LayoutInflater.from(p0.context).inflate(R.layout.list_material_detail,p0,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bindData(list[p1],listener)
    }

    inner class MyViewHolder(view:View):RecyclerView.ViewHolder(view){
        private var title:TextView = view.find(R.id.txtTitle)
        private var quiz:Button = view.find(R.id.btnQuiz)

        fun bindData(data:MaterialsItem,listener: (MaterialsItem) -> Unit){
            title.text = data.title
            quiz.setOnClickListener {
                listener(data)
            }
        }

    }
}