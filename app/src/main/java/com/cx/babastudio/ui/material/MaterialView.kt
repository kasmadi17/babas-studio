package com.cx.babastudio.ui.material

import com.cx.babastudio.model.material.Data
import com.cx.babastudio.ui.base.BaseView

interface MaterialView:BaseView {
    fun showMaterial(data:Data)
}