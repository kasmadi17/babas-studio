package com.cx.babastudio.ui.material

import android.support.design.theme.MaterialComponentsViewInflater
import com.cx.babastudio.model.material.Data
import com.cx.babastudio.model.material.Material
import com.cx.babastudio.network.ApiClient
import com.cx.babastudio.ui.base.BasePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MaterialPresenter:BasePresenter<MaterialView> {
    private var mView:MaterialView?=null
    override fun onAttach(view: MaterialView) {
        mView = view
    }

    override fun onDetach() {
        mView = null
    }

    fun getMaterial(header:String,idKursus:Int,idMaterial:Int){
        mView?.showProgress()
        val call:Call<Material> = ApiClient.retrofit.getMaterial(header,idKursus,idMaterial)
        call.enqueue(object : Callback<Material>{
            override fun onFailure(call: Call<Material>, t: Throwable) {
                mView?.hideProgress()
                mView?.showError()
            }

            override fun onResponse(call: Call<Material>, response: Response<Material>) {
                if (response.isSuccessful){
                    mView?.hideProgress()
                    val data = response.body()?.data
                    data?.let { mView?.showMaterial(it) }
                }
            }

        })
    }
}