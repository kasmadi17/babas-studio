package com.cx.babastudio.ui.base

interface BasePresenter<T:BaseView> {
    fun onAttach(view:T)
    fun onDetach()
}