package com.cx.babastudio.ui.material.materialdetail.quiz

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cx.babastudio.R
import com.cx.babastudio.model.material.QuizzesItem
import org.jetbrains.anko.find

class QuizAdapter(private val list: List<QuizzesItem>) : RecyclerView.Adapter<QuizAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view: View = LayoutInflater.from(p0.context).inflate(R.layout.list_quiz, p0, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bindData(list[p1])
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var question: TextView = view.find(R.id.txtQuestion)
        private var optionA: TextView = view.find(R.id.txtOptionA)
        private var optionB: TextView = view.find(R.id.txtOptionB)
        private var optionC: TextView = view.find(R.id.txtOptionC)
        private var optionD: TextView = view.find(R.id.txtOptionD)

        fun bindData(data:QuizzesItem){

            question.text = data.question
            optionA.text = "A. "+data.optionA
            optionB.text = "B. "+data.optionB
            optionC.text = "C. "+data.optionC
            optionD.text = "D. "+data.optionD
        }
    }
}