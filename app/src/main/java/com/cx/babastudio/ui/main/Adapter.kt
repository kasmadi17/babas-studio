package com.cx.babastudio.ui.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cx.babastudio.R
import com.cx.babastudio.model.JsonMember0
import com.cx.babastudio.model.PackagesItem
import com.cx.babastudio.util.currency
import org.jetbrains.anko.find

class Adapter(private val list:List<JsonMember0>,private val listener:(JsonMember0)->Unit)
    :RecyclerView.Adapter<Adapter.MyViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view:View = LayoutInflater.from(p0.context).inflate(R.layout.list_course,p0,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int =list.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bindData(list[p1],listener)
    }

    inner class MyViewHolder(view:View):RecyclerView.ViewHolder(view){
        private var title:TextView = view.find(R.id.txtTitle)
        private var duration:TextView = view.find(R.id.txtDuration)
        private var price:TextView = view.find(R.id.txtPrice)

        fun bindData(course:JsonMember0,listener: (JsonMember0) -> Unit){
            title.text = course.title
            val durationString = course.duration.toString() +" Menit"
            duration.text =durationString
            val priceString="Rp. " + currency(course.price.toString())
            price.text = priceString

            itemView.setOnClickListener {
                listener(course)
            }

        }

    }
}