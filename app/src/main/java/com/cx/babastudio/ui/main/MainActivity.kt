package com.cx.babastudio.ui.main

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.cx.babastudio.R
import com.cx.babastudio.model.GetViewCourse
import com.cx.babastudio.model.JsonMember0
import com.cx.babastudio.model.MyCourse
import com.cx.babastudio.ui.login.Login
import com.cx.babastudio.util.Constans.TOKEN
import com.cx.babastudio.util.SharePref
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.onRefresh


class MainActivity : AppCompatActivity(), MainView, AnkoLogger {


    private lateinit var presenter: MainPresenter
    private lateinit var sharePref: SharePref
    private lateinit var adapter: Adapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var listCourse: MutableList<JsonMember0> = mutableListOf()
    private var viewCourse:GetViewCourse?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    override fun showCourse(data: MyCourse) {
        listCourse.clear()
        data.data?.jsonMember0?.let { listCourse.add(it) }
        adapter.notifyDataSetChanged()
    }

    override fun showViewCourse(data: GetViewCourse) {
        viewCourse = data
        info("none $viewCourse")
    }

    override fun showProgress() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showError() {
        toast(R.string.error_msg)
    }

    override fun onAttachView() {
        presenter.onAttach(this)
        presenter.getCouser(sharePref.getString(TOKEN).toString())
    }

    override fun onDetachView() {
        presenter.onDetach()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInfaler = menuInflater
        menuInfaler.inflate(R.menu.main_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.logout->presenter.logout(sharePref.getString(TOKEN).toString())
        }
        return true
    }

    override fun logout() {
        sharePref.clear()
        startActivity<Login>()
        finish()
    }

    private fun init() {
        presenter = MainPresenter()
        sharePref = SharePref(this)
        recyclerView = find(R.id.recycleView)
        swipeRefreshLayout = find(R.id.swiperefresh)

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent, R.color.colorPrimary)
        swipeRefreshLayout.onRefresh {
            presenter.getCouser(sharePref.getString(TOKEN).toString())
        }

        adapter = Adapter(listCourse) {
            startActivity<com.cx.babastudio.ui.mycourse.MyCourse>("course" to it
                ,"data" to it.packages
                ,"viewCourse" to viewCourse)
        }

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter

        onAttachView()
    }
}
