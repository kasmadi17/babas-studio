package com.cx.babastudio.ui.mycourse

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.RecoverySystem
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.cx.babastudio.R
import com.cx.babastudio.model.GetViewCourse
import com.cx.babastudio.model.JsonMember0
import com.cx.babastudio.model.PackagesItem
import com.cx.babastudio.ui.main.MainActivity
import com.cx.babastudio.ui.material.Material
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.find
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivity

class MyCourse : AppCompatActivity(), AnkoLogger {
    private lateinit var adapter: MyCourseAdapter
    private lateinit var recyclerView: RecyclerView
    private var listData: MutableList<PackagesItem> = mutableListOf()
    private var materialId: Int? = null
    private var idKursus: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_course)
        init()
    }

    private fun init() {
        val course= intent.getParcelableExtra<JsonMember0>("course")
        val data = intent.getParcelableArrayListExtra<PackagesItem>("data")
        val viewCoures = intent.getParcelableExtra<GetViewCourse>("viewCourse")
        recyclerView = find(R.id.recycleView)
        adapter = MyCourseAdapter(listData) { _: Int, _: Int ->
            idKursus = course.id
            startActivity<Material>(
                "idMaterial" to viewCoures.pivot?.materialId,
                "idKursus" to idKursus
            )
        }


        materialId = viewCoures.pivot?.materialId
        listData.clear()
        listData.addAll(data)

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter

        adapter.notifyDataSetChanged()

    }
}
