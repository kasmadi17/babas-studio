package com.cx.babastudio.ui.material.materialdetail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.cx.babastudio.R
import com.cx.babastudio.model.material.MaterialsItem
import com.cx.babastudio.model.material.SectionsItem
import com.cx.babastudio.ui.material.materialdetail.quiz.Quiz
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity

class MaterialDetail : AppCompatActivity() {
    private lateinit var adapter: MaterialDetailAdapter
    private lateinit var recyclerView: RecyclerView
    private var listData:MutableList<MaterialsItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material_detail)
        init()
    }

    private fun init(){
        recyclerView = find(R.id.recycleView)
        adapter = MaterialDetailAdapter(listData){
            startActivity<Quiz>("data" to it)
        }

        val data = intent.getParcelableExtra<SectionsItem>("data")
        listData.clear()
        data.materials?.let { listData.addAll(it) }

        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = llm
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

    }
}
