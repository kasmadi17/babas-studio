package com.cx.babastudio.ui.login

import com.cx.babastudio.model.Users
import com.cx.babastudio.ui.base.BaseView

interface LoginView:BaseView {
    fun showData(data:Users)
    fun openHome()
}