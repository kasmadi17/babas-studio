package com.cx.babastudio.network

import com.cx.babastudio.model.*
import com.cx.babastudio.model.material.Material
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("username")username:String
              ,@Field("password")password:String)
            :Call<Users>

    @GET("social-learning-my-course")
    fun getCouser(@Header("Authorization")header: String)
            :Call<Course>

    @GET("social-learning-my-course/{id_kursus}/material/{id_material}")
    fun getMaterial(@Header("Authorization")header: String
                    ,@Path("id_kursus")idKursus:Int
                    ,@Path("id_material")idMaterial:Int):Call<Material>

    @POST("logout")
    fun logout(@Header("Authorization")header: String):Call<ResponseBody>

}