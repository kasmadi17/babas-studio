package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class Material(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: String? = null
)