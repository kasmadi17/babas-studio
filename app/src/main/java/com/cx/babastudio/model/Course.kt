package com.cx.babastudio.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Course(

    @field:SerializedName("search")
    val search: String? = null,

    @field:SerializedName("data_cuty")
    val dataCuty: String? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("get_view_course")
    val getViewCourse:@RawValue GetViewCourse? = null,

    @field:SerializedName("my_course")
    val myCourse:@RawValue MyCourse? = null,

    @field:SerializedName("last_viewed_videos")
    val lastViewedVideos:@RawValue LastViewedVideos? = null,

    @field:SerializedName("user")
    val user:@RawValue User? = null,

    @field:SerializedName("status_cuty")
    val statusCuty: Boolean? = null,

    @field:SerializedName("start_course")
    val startCourse:@RawValue List<String?>? = null,

    @field:SerializedName("tenant_id")
    val tenantId: Int? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("video")
    val video: String? = null,

    @field:SerializedName("meta_tag")
    val metaTag: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("wistia_hashed_link")
    val wistiaHashedLink: String? = null,

    @field:SerializedName("price_without_discount")
    val priceWithoutDiscount: Int? = null,

    @field:SerializedName("duration")
    val duration: Int? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("price")
    val price: Int? = null,

    @field:SerializedName("sub_category_id")
    val subCategoryId: String? = null,

    @field:SerializedName("certificate_title")
    val certificateTitle: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("is_publish")
    val isPublish: Int? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("meta_title")
    val metaTitle: String? = null,

    @field:SerializedName("can_be")
    val canBe: String? = null,

    @field:SerializedName("requirement")
    val requirement: String? = null,

    @field:SerializedName("wistia_hashed_id")
    val wistiaHashedId: String? = null,

    @field:SerializedName("meta_description")
    val metaDescription: String? = null,

    @field:SerializedName("user_id")
    val userId: Int? = null,

    @field:SerializedName("count_user")
    val countUser: Int? = null,

    @field:SerializedName("suitable")
    val suitable: String? = null,

    @field:SerializedName("count_video")
    val countVideo: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
):Parcelable