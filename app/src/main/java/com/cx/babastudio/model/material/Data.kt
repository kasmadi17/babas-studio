package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("quiz_data")
	val quizData: List<Any?>? = null,

	@field:SerializedName("material_lines")
	val materialLines: List<Int?>? = null,

	@field:SerializedName("top_progress")
	val topProgress: Double? = null,

	@field:SerializedName("material_data")
	val materialData: MaterialData? = null,

	@field:SerializedName("course_data")
	val courseData: CourseData? = null,

	@field:SerializedName("section_lines")
	val sectionLines: List<Int?>? = null,

	@field:SerializedName("req_ujian")
	val reqUjian: ReqUjian? = null
)