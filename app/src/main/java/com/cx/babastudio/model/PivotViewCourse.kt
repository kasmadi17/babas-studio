package com.cx.babastudio.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PivotViewCourse (

    @field:SerializedName("user_id")
    val userId: Int? = null,

    @field:SerializedName("material_id")
    val materialId: Int? = null,

    @field:SerializedName("created_at")
    val createAt: String? = null
):Parcelable