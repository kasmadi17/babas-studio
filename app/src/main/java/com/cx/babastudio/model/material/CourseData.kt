package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class CourseData(

	@field:SerializedName("tenant_id")
	val tenantId: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("video")
	val video: Any? = null,

	@field:SerializedName("meta_tag")
	val metaTag: Any? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("wistia_hashed_link")
	val wistiaHashedLink: Any? = null,

	@field:SerializedName("price_without_discount")
	val priceWithoutDiscount: Int? = null,

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("encounters")
	val encounters: List<EncountersItem?>? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("sub_category_id")
	val subCategoryId: Any? = null,

	@field:SerializedName("certificate_title")
	val certificateTitle: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("is_publish")
	val isPublish: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("meta_title")
	val metaTitle: Any? = null,

	@field:SerializedName("can_be")
	val canBe: Any? = null,

	@field:SerializedName("requirement")
	val requirement: Any? = null,

	@field:SerializedName("wistia_hashed_id")
	val wistiaHashedId: Any? = null,

	@field:SerializedName("sections")
	val sections: List<SectionsItem>? = null,

	@field:SerializedName("meta_description")
	val metaDescription: Any? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("count_user")
	val countUser: Int? = null,

	@field:SerializedName("suitable")
	val suitable: Any? = null,

	@field:SerializedName("count_video")
	val countVideo: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)