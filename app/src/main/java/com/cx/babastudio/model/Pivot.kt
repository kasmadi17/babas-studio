package com.cx.babastudio.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pivot(

	@field:SerializedName("course_id")
	val courseId: Int? = null,

	@field:SerializedName("course_package_id")
	val coursePackageId: Int? = null

):Parcelable