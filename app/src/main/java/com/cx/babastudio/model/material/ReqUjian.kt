package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class ReqUjian(

	@field:SerializedName("latihan")
	val latihan: Latihan? = null,

	@field:SerializedName("sertifikat")
	val sertifikat: List<Any?>? = null,

	@field:SerializedName("video")
	val video: Video? = null
)