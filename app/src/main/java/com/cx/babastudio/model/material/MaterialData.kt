package com.cx.babastudio.model.material


import com.google.gson.annotations.SerializedName

data class MaterialData(

	@field:SerializedName("line")
	val line: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("wistia_hashed_link")
	val wistiaHashedLink: String? = null,

	@field:SerializedName("wistia_hashed_id")
	val wistiaHashedId: Any? = null,

	@field:SerializedName("duration")
	val duration: Any? = null,

	@field:SerializedName("video_url")
	val videoUrl: Any? = null,

	@field:SerializedName("section_id")
	val sectionId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("quizzes")
	val quizzes: List<Any?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)