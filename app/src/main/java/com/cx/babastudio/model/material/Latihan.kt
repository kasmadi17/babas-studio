package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class Latihan(

	@field:SerializedName("txt")
	val txt: String? = null,

	@field:SerializedName("progress")
	val progress: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)