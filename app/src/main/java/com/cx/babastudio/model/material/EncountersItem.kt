package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class EncountersItem(

	@field:SerializedName("course_id")
	val courseId: Int? = null,

	@field:SerializedName("schedule")
	val schedule: List<ScheduleItem?>? = null,

	@field:SerializedName("is_active")
	val isActive: Int? = null,

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("is_download")
	val isDownload: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)