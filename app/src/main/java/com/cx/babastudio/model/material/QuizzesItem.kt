package com.cx.babastudio.model.material

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuizzesItem(

	@field:SerializedName("question")
	val question: String? = null,

	@field:SerializedName("line")
	val line: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("minute")
	val minute: Int? = null,

	@field:SerializedName("second")
	val second: Int? = null,

	@field:SerializedName("option_d")
	val optionD: String? = null,

	@field:SerializedName("option_b")
	val optionB: String? = null,

	@field:SerializedName("option_c")
	val optionC: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("option_true")
	val optionTrue: String? = null,

	@field:SerializedName("material_id")
	val materialId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("option_a")
	val optionA: String? = null
):Parcelable