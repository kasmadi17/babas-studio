package com.cx.babastudio.model

import com.google.gson.annotations.SerializedName

data class User(

	@field:SerializedName("tenant_id")
	val tenantId: Int? = null,

	@field:SerializedName("reason")
	val reason: String? = null,

	@field:SerializedName("notes")
	val notes: Any? = null,

	@field:SerializedName("tw")
	val tw: String? = null,

	@field:SerializedName("city")
	val city: Any? = null,

	@field:SerializedName("birth_date")
	val birthDate: Any? = null,

	@field:SerializedName("about")
	val about: Any? = null,

	@field:SerializedName("is_partnership")
	val isPartnership: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("hope")
	val hope: Any? = null,

	@field:SerializedName("date_expired")
	val dateExpired: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("role_id")
	val roleId: Int? = null,

	@field:SerializedName("branch_id")
	val branchId: Int? = null,

	@field:SerializedName("mobile2")
	val mobile2: Any? = null,

	@field:SerializedName("skill")
	val skill: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("ktp_id")
	val ktpId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("ig")
	val ig: String? = null,

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("point_id")
	val pointId: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("sex")
	val sex: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("verified")
	val verified: Int? = null,

	@field:SerializedName("photo")
	val photo: Any? = null,

	@field:SerializedName("email_verified_at")
	val emailVerifiedAt: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: Any? = null,

	@field:SerializedName("pin_bb")
	val pinBb: Any? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("provider_id")
	val providerId: Any? = null,

	@field:SerializedName("fullname")
	val fullname: String? = null,

	@field:SerializedName("fb")
	val fb: String? = null,

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("hobby")
	val hobby: Any? = null
)