package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class Video(

	@field:SerializedName("txt")
	val txt: String? = null,

	@field:SerializedName("progress")
	val progress: Double? = null,

	@field:SerializedName("status")
	val status: Int? = null
)