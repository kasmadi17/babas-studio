package com.cx.babastudio.model.material

import com.google.gson.annotations.SerializedName

data class ScheduleItem(

	@field:SerializedName("schedule")
	val schedule: String? = null,

	@field:SerializedName("e_flag")
	val eFlag: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("encounter_id")
	val encounterId: Int? = null,

	@field:SerializedName("test_to")
	val testTo: Int? = null
)