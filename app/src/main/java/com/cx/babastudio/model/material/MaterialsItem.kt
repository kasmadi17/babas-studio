package com.cx.babastudio.model.material

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class MaterialsItem(

	@field:SerializedName("line")
	val line: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("wistia_hashed_link")
	val wistiaHashedLink: String? = null,

	@field:SerializedName("wistia_hashed_id")
	val wistiaHashedId: String? = null,

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("video_url")
	val videoUrl: String? = null,

	@field:SerializedName("section_id")
	val sectionId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("quizzes")
	val quizzes:@RawValue List<QuizzesItem>? = null,

	@field:SerializedName("status")
	val status: Int? = null
):Parcelable