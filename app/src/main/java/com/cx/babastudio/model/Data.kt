package com.cx.babastudio.model

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("0")
	val jsonMember0: JsonMember0? = null,

	@field:SerializedName("2")
	val jsonMember2: JsonMember2? = null,

	@field:SerializedName("3")
	val jsonMember3: JsonMember3? = null,

	@field:SerializedName("4")
	val jsonMember4: JsonMember4? = null,

	@field:SerializedName("5")
	val jsonMember5: JsonMember5? = null,

	@field:SerializedName("6")
	val jsonMember6: JsonMember6? = null
)