package com.cx.babastudio.model.material

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class SectionsItem(

	@field:SerializedName("course_id")
	val courseId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("exercises")
	val exercises:@RawValue List<ExercisesItem>? = null,

	@field:SerializedName("materials")
	val materials:@RawValue List<MaterialsItem>? = null,

	@field:SerializedName("essays")
	val essays:@RawValue List<Any>? = null,

	@field:SerializedName("line")
	val line: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("wistia_project_id")
	val wistiaProjectId: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
):Parcelable