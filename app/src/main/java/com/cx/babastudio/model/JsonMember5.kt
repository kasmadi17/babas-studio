package com.cx.babastudio.model

import com.google.gson.annotations.SerializedName

data class JsonMember5(

	@field:SerializedName("tenant_id")
	val tenantId: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("packages")
	val packages: List<Any?>? = null,

	@field:SerializedName("price_without_discount")
	val priceWithoutDiscount: Int? = null,

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("sub_category_id")
	val subCategoryId: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)